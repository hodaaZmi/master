QT += core
QT -= gui

TARGET = master
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    sender.cpp \
    receiver.cpp

HEADERS += \
    sender.h \
    receiver.h
QT           += network

